﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAutomatization.Entity
{
    public class FinReport
    {
        public int ReportID { get; set; }
        public Professor Professor { get; set; }
        public DateTime CreationDate { get; set; }
        public Guid ReportGUID { get; set; }
        public int Salary 
        {
            get {
                int sal = 0;
                if (SubReports == null)
                    sal = 0;
                else
                {
                    foreach (SubReport sr in SubReports)
                    {
                        if(sr.Loading  != null && sr.Payment != null)
                            sal += sr.Loading.Hours * sr.Payment.PayPerHour;
                    }
                }
                return sal;

            }
        }

        public List<SubReport> SubReports { get; set; }
    }
}
