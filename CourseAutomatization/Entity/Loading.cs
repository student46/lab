﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAutomatization.Entity
{
    public class Loading
    {
        public int LoadID { get; set; }

        public Professor Professor { get; set; }
        public Group Group { get; set; }

        public int Hours { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Professor, Group, Hours);
        }

    }
}
