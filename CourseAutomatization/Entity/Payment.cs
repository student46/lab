﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAutomatization.Entity
{
    public class Payment
    {
        public int PaymentID { get; set; }
        public int PayPerHour { get; set; }

        public override string ToString()
        {
            return PayPerHour.ToString();
        }
    }
}
