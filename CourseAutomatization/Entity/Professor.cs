﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAutomatization.Entity
{
    public class Professor
    {
        public int ProfID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronimyc { get; set; }
        public int Age { get; set; }
        public int Experience { get; set; }
        public string PaspSeries{ get; set; }
        public string PaspNumber { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", FirstName, LastName, Patronimyc);
        }

    }
}
