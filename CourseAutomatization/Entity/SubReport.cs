﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseAutomatization.Entity
{
    public class SubReport
    {
        public int SubReportID { get; set; }
        public Loading Loading { get; set; }
        public Payment Payment { get; set; }

        public Guid ReportGUID { get; set; }

    }
}
