﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CourseAutomatization
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void профессорыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProfList profList = new frmProfList();
            profList.MdiParent = this;
            profList.Show();
        }

        private void группыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmGroupList groupList = new frmGroupList();
            groupList.MdiParent = this;
            groupList.Show();
        }

        private void оплатаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPaymentList paymentList = new frmPaymentList();
            paymentList.MdiParent = this;
            paymentList.Show();

        }

        private void вводДанныхToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLoadingList loadingList = new frmLoadingList();
            loadingList.MdiParent = this;
            loadingList.Show();
        }

        private void финансовыеОтчетыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFinReportList reportlist = new frmFinReportList();
            reportlist.MdiParent = this;
            reportlist.Show();
        }
    }
}
