﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using CourseAutomatization.Entity;

namespace CourseAutomatization.Repository
{
    public class FinReportRepository
    {
        //получаем список фин. документов
        public List<FinReport> GetFinReportList()
        {
            LoadingRepository loadingRepo = new LoadingRepository();
            PaymentRepository paymentRepo = new PaymentRepository();
            ProfessorRepository profRepo = new ProfessorRepository();

            List<Loading> loadingList = loadingRepo.GetLoadingList();
            List<Payment> paymentList = paymentRepo.GetpaymentList();
            List<Professor> profList = profRepo.GetProfList();

            List<FinReport> result = new List<FinReport>();
            string sql = @"SELECT
                                ReportID,
                                ProfID,
                                CreationDate,
                                ReportGUID
                            FROM
                                FinReport";

            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(com);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                FinReport report = new FinReport();
                report.SubReports = new List<SubReport>();
                report.ReportID = Convert.ToInt32(row["ReportID"]);

                int ProfID = Convert.ToInt32(row["ProfID"]);
                string guid = Convert.ToString(row["ReportGUID"]);
                DateTime creationDate = Convert.ToDateTime(row["CreationDate"]);
                Professor pr = profList.Where(x => x.ProfID == ProfID).FirstOrDefault();
                report.ReportGUID = Guid.Parse(guid);

                report.Professor = pr;
                report.CreationDate = creationDate;

                sql = @"SELECT
                                SubReportID,
                                LoadID,
                                PaymentID
                            FROM
                                SubReport
                            WHERE ReportGUID = :ReportGUID";

                com = new SQLiteCommand(sql, DBConnection.con);
                com.Parameters.AddWithValue("ReportGUID", report.ReportGUID.ToString());
                SQLiteDataAdapter dataAdapterSub = new SQLiteDataAdapter(com);
                DataTable dataTableSub = new DataTable();
                dataAdapterSub.Fill(dataTableSub);

                foreach (DataRow rowSub in dataTableSub.Rows)
                {
                    SubReport sub = new SubReport();

                    int SubReportID = Convert.ToInt32(rowSub["SubReportID"]);
                    int loadingID = Convert.ToInt32(rowSub["LoadID"]);
                    int paymentID = Convert.ToInt32(rowSub["PaymentID"]);

                    Payment p = paymentList.Where(x => x.PaymentID == paymentID).FirstOrDefault();
                    Loading l = loadingList.Where(x => x.LoadID == loadingID).FirstOrDefault();

                    sub.Loading = l;
                    sub.Payment = p;
                    sub.SubReportID = SubReportID;
                    sub.ReportGUID = Guid.Parse(guid);

                    report.SubReports.Add(sub);
                }
                result.Add(report);
            }

            return result;
        }

        public void AddFinReport(FinReport report)
        {
            string sql = @"INSERT INTO FinReport (
                                ProfID,
                                CreationDate,
                                ReportGUID
                            ) VALUES (
                                :ProfID,
                                :CreationDate,
                                :ReportGUID
                            )";
            //создаем запрос на добавление записи
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", report.Professor.ProfID);
            com.Parameters.AddWithValue("CreationDate", report.CreationDate);
            com.Parameters.AddWithValue("ReportGUID", report.ReportGUID.ToString());


            //выполняем
            com.ExecuteNonQuery();

            foreach (SubReport sr in report.SubReports)
            {
                sql = @"INSERT INTO SubReport (
                                LoadId,
                                PaymentID,
                                ReportGUID
                            ) VALUES (
                                :LoadId,
                                :PaymentID,
                                :ReportGUID
                            )";
                //создаем запрос на добавление записи
                com = new SQLiteCommand(sql, DBConnection.con);

                //заполняем параметрами
                com.Parameters.AddWithValue("LoadId", sr.Loading.LoadID);
                com.Parameters.AddWithValue("PaymentID", sr.Payment.PaymentID);
                com.Parameters.AddWithValue("ReportGUID", report.ReportGUID.ToString());


                //выполняем
                com.ExecuteNonQuery();
            }




        }


        //Метод для редактирования отчета
        public void EditLoading(FinReport report)
        {
            string sql = @"update FinReport set
                                ProfID = :ProfID
                            where ReportID = :ReportID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", report.Professor.ProfID);
            com.Parameters.AddWithValue("ReportID", report.ReportID);

            //выполняем запрос
            com.ExecuteNonQuery();


             sql = @"delete from SubReport 
                            where ReportGUID = :ReportGUID";
            //создаем запрос
            com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ReportGUID", report.ReportGUID.ToString());
            com.ExecuteNonQuery();

            foreach (SubReport sr in report.SubReports)
            {
                sql = @"INSERT INTO SubReport (
                                LoadId,
                                PaymentID,
                                ReportGUID
                            ) VALUES (
                                :LoadId,
                                :PaymentID,
                                :ReportGUID
                            )";
                //создаем запрос на добавление записи
                com = new SQLiteCommand(sql, DBConnection.con);

                //заполняем параметрами
                com.Parameters.AddWithValue("LoadId", sr.Loading.LoadID);
                com.Parameters.AddWithValue("PaymentID", sr.Payment.PaymentID);
                com.Parameters.AddWithValue("ReportGUID", report.ReportGUID.ToString());


                //выполняем
                com.ExecuteNonQuery();
            }


            //выполняем запрос
            
        }


        //Метод для удаления отчета
        public void DeleteReport(FinReport report)
        {
            string sql = @"delete from FinReport 
                            where ReportID = :ReportID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ReportID", report.ReportID);

            //выполняем запрос
            com.ExecuteNonQuery();

        }
    }
}
