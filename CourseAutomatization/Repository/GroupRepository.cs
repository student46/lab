﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using CourseAutomatization.Entity;

namespace CourseAutomatization.Repository
{
    public class GroupRepository
    {
        //метод для получения списка групп
        public List<Group> GetGroupList()
        {

            List<Group> result = new List<Group>();
            string sql = @"SELECT
                                GroupID,
                                StudentCount,
                                Speciality 
                            FROM
                                Groups";
            //создаем запрос к БД на выбов всех групп
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(com);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                Group group = new Group();
                group.GroupID = Convert.ToInt32(row["GroupID"]);
                group.StudentCount = Convert.ToInt32(row["StudentCount"]);
                group.Speciality = Convert.ToString(row["Speciality"]);
                
                result.Add(group);
            }

            return result;
        }

        //метод для добавления группы
        public void AddGroup(Group group)
        {
            string sql = @"INSERT INTO Groups (
                                StudentCount,
                                Speciality
                            ) VALUES (
                                :StudentCount,
                                :Speciality
                            )";
            //создаем запрос на добавление записи
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("StudentCount", group.StudentCount);
            com.Parameters.AddWithValue("Speciality", group.Speciality);

            //выполняем
            com.ExecuteNonQuery();
        }

        //Метод для редактирования групп
        public void EditGroup(Group group)
        {
            string sql = @"update Groups set
                                StudentCount = :StudentCount,
                                Speciality = :Speciality
                            where GroupID = :GroupID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("StudentCount", group.StudentCount);
            com.Parameters.AddWithValue("Speciality", group.Speciality);
            com.Parameters.AddWithValue("GroupID", group.GroupID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }

        //Метод для удаления групп
        public void DeleteGroup(Group group)
        {
            string sql = @"delete from Groups 
                            where GroupID = :GroupID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("GroupID", group.GroupID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }
    }
}
