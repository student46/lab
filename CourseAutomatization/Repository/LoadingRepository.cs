﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using CourseAutomatization.Entity;

namespace CourseAutomatization.Repository
{
    public class LoadingRepository
    {
        public List<Loading> GetLoadingList()
        {
            ProfessorRepository profRepo = new ProfessorRepository();
            GroupRepository groupRepo = new GroupRepository();

            List<Professor> profList = profRepo.GetProfList();
            List<Group> groupList = groupRepo.GetGroupList();


            List<Loading> result = new List<Loading>();
            string sql = @"SELECT
                                LoadID,
                                ProfID,
                                GroupID,
                                Hours
                            FROM
                                Loadings";
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(com);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                Loading load = new Loading();
                load.LoadID = Convert.ToInt32(row["LoadID"]);
                load.Hours = Convert.ToInt32(row["Hours"]);

                int profId = Convert.ToInt32(row["ProfID"]);
                int groupId = Convert.ToInt32(row["GroupID"]);

                Group g = groupList.Where(x => x.GroupID == groupId).FirstOrDefault();
                Professor p = profList.Where(x => x.ProfID == profId).FirstOrDefault();

                load.Group = g;
                load.Professor = p;

                result.Add(load);
            }

            return result;
        }

        public void AddLoading(Loading loading)
        {
            string sql = @"INSERT INTO Loadings (
                                ProfID,
                                GroupID,
                                Hours
                            ) VALUES (
                                :ProfID,
                                :GroupID,
                                :Hours
                            )";
            //создаем запрос на добавление записи
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", loading.Professor.ProfID);
            com.Parameters.AddWithValue("GroupID", loading.Group.GroupID);
            com.Parameters.AddWithValue("Hours", loading.Hours);


            //выполняем
            com.ExecuteNonQuery();
        }


        //Метод для редактирования нагрузки
        public void EditLoading(Loading loading)
        {
            string sql = @"update Loadings set
                                ProfID = :ProfID,
                                GroupID = :GroupID,
                                Hours = :Hours
                            where LoadID = :LoadID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", loading.Professor.ProfID);
            com.Parameters.AddWithValue("GroupID", loading.Group.GroupID);
            com.Parameters.AddWithValue("Hours", loading.Hours);
            com.Parameters.AddWithValue("LoadID", loading.LoadID);


            //выполняем запрос
            com.ExecuteNonQuery();
        }


        //Метод для редактирования нагрузки
        public void DeleteLoading(Loading loading)
        {
            string sql = @"delete from Loadings 
                            where LoadID = :LoadID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("LoadID", loading.LoadID);

            //выполняем запрос
            com.ExecuteNonQuery();

            //удаляем так же из FinReport
            sql = @"delete from FinReport 
                            where LoadID = :LoadID";
            //создаем запрос
            com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("LoadID", loading.LoadID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }
    }
}
