﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using CourseAutomatization.Entity;

namespace CourseAutomatization.Repository
{
    public class PaymentRepository
    {
        //метод для получения списка оплат
        public List<Payment> GetpaymentList()
        {

            List<Payment> result = new List<Payment>();
            string sql = @"SELECT
                                PaymentID,
                                PayPerHour
                            FROM
                                Payments";
            //создаем запрос к БД на выбов всех оплат
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(com);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                Payment pay = new Payment();
                pay.PaymentID = Convert.ToInt32(row["PaymentID"]);
                pay.PayPerHour = Convert.ToInt32(row["PayPerHour"]);


                result.Add(pay);
            }

            return result;
        }

        //метод для добавления оплатыы
        public void AddPayment(Payment pay)
        {
            string sql = @"INSERT INTO Payments (
                                PayPerHour
                            ) VALUES (
                                :PayPerHour
                            )";
            //создаем запрос на добавление записи
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("PayPerHour", pay.PayPerHour);

            //выполняем
            com.ExecuteNonQuery();
        }

        //Метод для редактирования оплаты
        public void EditPayment(Payment pay)
        {
            string sql = @"update Payments set
                                PayPerHour = :PayPerHour
                            where PaymentID = :PaymentID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("PayPerHour", pay.PayPerHour);
            com.Parameters.AddWithValue("PaymentID", pay.PaymentID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }

        //Метод для удалениея оплаты
        public void DeletePayment(Payment pay)
        {
            string sql = @"delete from Payments 
                            where PaymentID = :PaymentID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("PaymentID", pay.PaymentID);

            //выполняем запрос
            com.ExecuteNonQuery();

            //удаляем так же из таблицы FinReport
            sql = @"delete from FinReport 
                            where PaymentID = :PaymentID";
            //создаем запрос
            com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("PaymentID", pay.PaymentID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }
    }
}
