﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using CourseAutomatization.Entity;

namespace CourseAutomatization.Repository
{
    public class ProfessorRepository
    {
        //метод для получения спсика профессоров
        public List<Professor> GetProfList()
        {

            List<Professor> result = new List<Professor>();
            string sql = @"SELECT
                                ProfID,
                                FirstName,
                                LastName,
                                Patronymic,
                                Age,
                                Experience,
                                PaspSeries,
                                PaspNumber,
                                Address,
                                Phone
                            FROM
                                Professors";
            //создаем запрос к БД на выбов всех профессоров
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);
            SQLiteDataAdapter dataAdapter = new SQLiteDataAdapter(com);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            foreach (DataRow row in dataTable.Rows)
            {
                Professor prof = new Professor();
                prof.ProfID = Convert.ToInt32(row["ProfID"]);
                prof.FirstName = Convert.ToString(row["FirstName"]);
                prof.LastName = Convert.ToString(row["LastName"]);
                prof.Patronimyc = Convert.ToString(row["Patronymic"]);
                prof.Age = Convert.ToInt32(row["Age"]);
                prof.Experience = Convert.ToInt32(row["Experience"]);
                prof.PaspSeries = Convert.ToString(row["PaspSeries"]);
                prof.PaspNumber = Convert.ToString(row["PaspNumber"]);
                prof.Address = Convert.ToString(row["Address"]);
                prof.Phone = Convert.ToString(row["Phone"]);
                result.Add(prof);
            }

            return result;
        }

        //метод для добавления профессора
        public void AddProfessor(Professor prof)
        {
            string sql = @"INSERT INTO Professors (
                                FirstName,
                                LastName,
                                Patronymic,
                                Age,
                                Experience,
                                PaspSeries,
                                PaspNumber,
                                Address,
                                Phone
                            ) VALUES (
                                :FirstName,
                                :LastName,
                                :Patronymic,
                                :Age,
                                :Experience,
                                :PaspSeries,
                                :PaspNumber,
                                :Address,
                                :Phone
                            )";
            //создаем запрос на добавление записи
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("FirstName", prof.FirstName);
            com.Parameters.AddWithValue("LastName", prof.LastName);
            com.Parameters.AddWithValue("Patronymic", prof.Patronimyc);
            com.Parameters.AddWithValue("Age", prof.Age);
            com.Parameters.AddWithValue("Experience", prof.Experience);
            com.Parameters.AddWithValue("PaspSeries", prof.PaspSeries);
            com.Parameters.AddWithValue("PaspNumber", prof.PaspNumber);
            com.Parameters.AddWithValue("Address", prof.Address);
            com.Parameters.AddWithValue("Phone", prof.Phone);

            //выполняем
            com.ExecuteNonQuery();
        }

        //Метод для редактирования профессора
        public void EditProfessor(Professor prof)
        {
            string sql = @"update Professors set
                                FirstName = :FirstName,
                                LastName = :LastName,
                                Patronymic = :Patronymic,
                                Age = :Age,
                                Experience = :Experience,
                                PaspSeries = :PaspSeries,
                                PaspNumber = :PaspNumber,
                                Address = :Address,
                                Phone = :Phone 
                            where ProfID = :ProfID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("FirstName", prof.FirstName);
            com.Parameters.AddWithValue("LastName", prof.LastName);
            com.Parameters.AddWithValue("Patronymic", prof.Patronimyc);
            com.Parameters.AddWithValue("Age", prof.Age);
            com.Parameters.AddWithValue("Experience", prof.Experience);
            com.Parameters.AddWithValue("PaspSeries", prof.PaspSeries);
            com.Parameters.AddWithValue("PaspNumber", prof.PaspNumber);
            com.Parameters.AddWithValue("Address", prof.Address);
            com.Parameters.AddWithValue("Phone", prof.Phone);
            com.Parameters.AddWithValue("ProfID", prof.ProfID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }

        //метод для удаления преподавателей
        public void DeleteProfessor(Professor prof)
        {
            string sql = @"delete from Professors
                            where ProfID = :ProfID";
            //создаем запрос
            SQLiteCommand com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", prof.ProfID);

            //выполняем запрос
            com.ExecuteNonQuery();

            //удаляем так же из Loadings
            sql = @"delete from Loadings
                            where ProfID = :ProfID";
            //создаем запрос
            com = new SQLiteCommand(sql, DBConnection.con);

            //заполняем параметрами
            com.Parameters.AddWithValue("ProfID", prof.ProfID);

            //выполняем запрос
            com.ExecuteNonQuery();
        }
    }
    

}

