﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmFinReportEdit : Form
    {
        FinReport report;               //объект отчета
        FinReportRepository reportRepo; //класс для работы с фин документами
        LoadingRepository loadingRepo;  //класс для работы с нагрузкой
        PaymentRepository paymentRepo;      //класс для работы с оплатой
        ProfessorRepository profRepo;      //класс для работы с оплатой
        public frmFinReportEdit()
        {
            InitializeComponent();
        }
        public frmFinReportEdit(FinReportRepository _reportRepo)
        {
            InitializeComponent();
            report = new FinReport();
            report.SubReports = new List<SubReport>();

            reportRepo = _reportRepo;
            loadingRepo = new LoadingRepository();
            paymentRepo = new PaymentRepository();
            profRepo = new ProfessorRepository();

            //заполняем выподающие списки данными
            cmbLoading.DataSource = loadingRepo.GetLoadingList();
            cmbPayment.DataSource = paymentRepo.GetpaymentList();
            cmbProf.DataSource = profRepo.GetProfList();
        }

        public frmFinReportEdit(FinReportRepository _reportRepo, FinReport _report)
        {
            InitializeComponent();

            report = _report;

            reportRepo = _reportRepo;
            loadingRepo = new LoadingRepository();
            paymentRepo = new PaymentRepository();
            profRepo = new ProfessorRepository();

            //заполняем выподающие списки данными
            cmbLoading.DataSource = loadingRepo.GetLoadingList();
            cmbPayment.DataSource = paymentRepo.GetpaymentList();
            cmbProf.DataSource = profRepo.GetProfList();

            //если открываем форму для редактирования, заполняем поля
            if (report.Professor != null)
            {
                if (report.Professor != null)
                {
                    for (int i = 0; i < cmbProf.Items.Count; i++)
                    {
                        if (((cmbProf.Items[i]) as Professor).ProfID == report.Professor.ProfID)
                            cmbProf.SelectedIndex = i;
                    }
                }

                foreach (SubReport sr in report.SubReports)
                {
                    SubReport sub = new SubReport();
                    sub.Payment = sr.Payment;
                    sub.Loading = sr.Loading;
                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = sub.Loading.Group.Speciality;
                    lvi.SubItems.Add(sub.Payment.ToString());
                    lvi.Tag = sub;
                    listView1.Items.Add(lvi);
 
                }
                 
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //проверка данных
            if (CheckData())
            {
                //если объект пустой, значит это создание нового документа
                if (report.ReportGUID == Guid.Empty)
                {
                    report.Professor = (cmbProf.SelectedItem as Professor);
                    report.CreationDate = DateTime.Now;
                    report.ReportGUID = Guid.NewGuid();

                    //добавление в БД
                    reportRepo.AddFinReport(report);
                }
                else
                {
                    //иначе изменение текущего
                    report.Professor = (cmbProf.SelectedItem as Professor);

                    //изменение в БД
                    reportRepo.EditLoading(report);
                }
            }
            else
            {
                MessageBox.Show("Данные внесены некорректно!");
            }
        }

        private bool CheckData()
        {
            //проверяем, выбраны ли значения
            bool result = true;
            if (cmbProf.SelectedIndex == -1)
                result = false;
            if (report.SubReports.Count <= 0)
                result = false;

            return result;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SubReport sub = new SubReport();
            sub.Payment = (cmbPayment.SelectedItem as Payment);
            sub.Loading = (cmbLoading.SelectedItem as Loading);
            ListViewItem lvi = new ListViewItem();
            lvi.Text = sub.Loading.Group.Speciality;
            lvi.SubItems.Add(sub.Payment.ToString());
            lvi.Tag = sub;
            listView1.Items.Add(lvi);
            report.SubReports.Add(sub);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                SubReport sr = (listView1.SelectedItems[0].Tag as SubReport);
                report.SubReports.Remove(report.SubReports.Where(x => x.Loading.LoadID == sr.Loading.LoadID && x.Payment.PaymentID == sr.Payment.PaymentID).First());
                listView1.SelectedItems[0].Remove();
            }
        }
    }
}
