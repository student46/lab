﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;
using Xceed.Words.NET;
using System.Diagnostics;

namespace CourseAutomatization
{
    public partial class frmFinReportList : Form
    {
        FinReportRepository reportRepo = new FinReportRepository();
        public frmFinReportList()
        {
            InitializeComponent();
        }

        private void LoadData()
        {

            dgvList.DataSource = reportRepo.GetFinReportList();
            dgvList.Columns["ReportID"].HeaderText = "ID";
            dgvList.Columns["Professor"].HeaderText = "Преподаватель";
            dgvList.Columns["CreationDate"].HeaderText = "Дата создания";
            dgvList.Columns["Salary"].HeaderText = "Заработная плата";

            dgvList.Columns["ReportGUID"].Visible = false;
            dgvList.Columns["ReportID"].Visible = false;

            dgvList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
        private void frmFinReportList_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmFinReportEdit reportEdit = new frmFinReportEdit(reportRepo);
            if (reportEdit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                FinReport report = (dgvList.SelectedRows[0].DataBoundItem as FinReport);
                frmFinReportEdit reportEdit = new frmFinReportEdit(reportRepo, report);
                if (reportEdit.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                FinReport report = (dgvList.SelectedRows[0].DataBoundItem as FinReport);
                GenerateFinReport(report);
            }
        }

        private void GenerateFinReport(FinReport report)
        {
            if (report != null)
            {
                string fileName = System.IO.Path.Combine(Environment.CurrentDirectory, "exempleReport.docx");

                var doc = DocX.Create(fileName);

                doc.InsertParagraph("ФИНАНСОВЫЙ ОТЧЕТ");

                doc.InsertParagraph("№ " + report.ReportID.ToString("00000"));
                doc.InsertParagraph("Преподаватель: " + report.Professor.ToString());
                doc.InsertParagraph("Дата формирования отчета: " + report.CreationDate.ToString());
                foreach (SubReport sr in report.SubReports)
                {
                    doc.InsertParagraph("Группа :" + sr.Loading.Group.ToString().PadLeft(50, ' ') + ", оплата: " + (sr.Payment.PayPerHour * sr.Loading.Hours).ToString().PadLeft(5, ' '));
                    //doc.InsertParagraph("Группа :" + report.Loading.Group.ToString());
                    //doc.InsertParagraph("Ставка :" + report.Payment.PayPerHour);
                    //doc.InsertParagraph("Заработная плата :" + report.Loading.Hours * report.Payment.PayPerHour);
                }
                doc.InsertParagraph("ИТОГО: " + report.Salary.ToString().PadLeft(55, ' '));
                doc.Save();

                Process.Start("WINWORD.EXE", fileName);
            }
        }
    }
}
