﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmGroupEdit : Form
    {
        Group group;        //объект группы
        GroupRepository groupRepo;      //класс для работы с группой
        public frmGroupEdit()
        {
            InitializeComponent();
        }
        public frmGroupEdit(GroupRepository _groupRepo)
        {
            InitializeComponent();
            groupRepo = _groupRepo;
        }

        public frmGroupEdit(GroupRepository _groupRepo, Group _group)
        {
            InitializeComponent();
            group = _group;
            groupRepo = _groupRepo;

            //если открываем форму для редактирования, заполняем поля
            if (group != null)
            {
                txtStudentCount.Text = group.StudentCount.ToString();
                txtSpeciality.Text = group.Speciality;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //проверяем данных
            if (CheckData())
            {
                //если объект пустой, значит это создание нового документа
                if (group == null)
                {
                    group = new Group();

                    group.StudentCount = Convert.ToInt32(txtStudentCount.Text);
                    group.Speciality = txtSpeciality.Text;

                    //добавляем в БД
                    groupRepo.AddGroup(group);
                }
                else
                {
                    //иначе измененяем текущий
                    group.StudentCount = Convert.ToInt32(txtStudentCount.Text);
                    group.Speciality = txtSpeciality.Text;

                    //сохраянем изменение в БД
                    groupRepo.EditGroup(group);
                }
            }
            else
            {
                MessageBox.Show("Данные внесены некорректно!");
            }
        }

        private bool CheckData()
        {
            //проверяем, выбраны ли значения
            bool result = true;
            if (txtSpeciality.Text.Trim() == "")
                result = false;

            //проверка поля количество на число и вменяемый размер
            string str = txtStudentCount.Text;
            int count = 0;
            if (int.TryParse(str, out count))
            {
                if (count < 0 || count > 100)
                    result = false;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}
