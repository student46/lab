﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmGroupList : Form
    {
        GroupRepository groupRepo = new GroupRepository();
        public frmGroupList()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            //получем список групп
            dgvList.DataSource = groupRepo.GetGroupList();
            //меняем названия столбцов
            dgvList.Columns["GroupID"].HeaderText = "ID";
            dgvList.Columns["StudentCount"].HeaderText = "Количество студентов";
            dgvList.Columns["Speciality"].HeaderText = "Специальность";
 
            //убираем видимость столбца с иждентефикатором
            dgvList.Columns["GroupID"].Visible = false;

            // выставляем режим ширины столбцов 
            dgvList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
        private void frmGroupList_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            //создаем форму
            frmGroupEdit groupEdit = new frmGroupEdit(groupRepo);
            if (groupEdit.ShowDialog() == DialogResult.OK)
            {
                //если в ней была нажата кнопка сохранить - обновляем список
                LoadData();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void EditRecord()
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                //получаем выбраную запись в datagridview
                Group group = (dgvList.SelectedRows[0].DataBoundItem as Group);
                //передаем в форму для редактирования
                frmGroupEdit groupEdit = new frmGroupEdit(groupRepo, group);
                if (groupEdit.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
            }
        }
    }
}
