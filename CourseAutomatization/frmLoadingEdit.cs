﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmLoadingEdit : Form
    {
        Loading loading;
        LoadingRepository loadingRepo;
        ProfessorRepository profRepo;
        GroupRepository groupRepo;

        public frmLoadingEdit()
        {
            InitializeComponent();
        }
        public frmLoadingEdit(LoadingRepository _loadingRepo)
        {
            InitializeComponent();

            loadingRepo = _loadingRepo;
            profRepo = new ProfessorRepository();
            groupRepo = new GroupRepository();

            cmbProfessor.DataSource = profRepo.GetProfList();
            cmbGroup.DataSource = groupRepo.GetGroupList();
        }

        public frmLoadingEdit(LoadingRepository _loadingRepo, Loading _loading)
        {
            InitializeComponent();
            loadingRepo = _loadingRepo;
            profRepo = new ProfessorRepository();
            groupRepo = new GroupRepository();

            loading = _loading;

            cmbProfessor.DataSource = profRepo.GetProfList();
            cmbGroup.DataSource = groupRepo.GetGroupList();

            if (loading != null)
            {
                if (loading.Group != null)
                {
                    for (int i = 0; i < cmbGroup.Items.Count; i++)
                    {
                        if (((cmbGroup.Items[i]) as Group).GroupID == loading.Group.GroupID)
                            cmbGroup.SelectedIndex = i;
                    }
                }

                if (loading.Professor != null)
                {
                    for (int i = 0; i < cmbProfessor.Items.Count; i++)
                    {
                        if (((cmbProfessor.Items[i]) as Professor).ProfID == loading.Professor.ProfID)
                            cmbProfessor.SelectedIndex = i;
                    }
                }

                txtHours.Text = loading.Hours.ToString();
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckData())
            {
                if (loading == null)
                {
                    loading = new Loading();

                    loading.Hours = Convert.ToInt32(txtHours.Text);
                    loading.Professor = (cmbProfessor.SelectedItem as Professor);
                    loading.Group = (cmbGroup.SelectedItem as Group);

                    loadingRepo.AddLoading(loading);
                }
                else
                {
                    loading.Hours = Convert.ToInt32(txtHours.Text);
                    loading.Professor = (cmbProfessor.SelectedItem as Professor);
                    loading.Group = (cmbGroup.SelectedItem as Group);

                    loadingRepo.EditLoading(loading);
                }
            }
            else
            {
                MessageBox.Show("Данные внесены некорректно!");
            }
        }

        private bool CheckData()
        {
            bool result = true;
            if (cmbProfessor.SelectedIndex == -1)
                result = false;
            if (cmbGroup.SelectedIndex == -1)
                result = false;

            string str = txtHours.Text;
            int hours = 0;
            if (int.TryParse(str, out hours))
            {
                if (hours < 0 || hours > 30 * 24)
                    result = false;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}

