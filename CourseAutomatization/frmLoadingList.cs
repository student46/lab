﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmLoadingList : Form
    {
        LoadingRepository loadingRepo = new LoadingRepository();
        public frmLoadingList()
        {
            InitializeComponent();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            frmLoadingEdit loadingEdit = new frmLoadingEdit(loadingRepo);
            if (loadingEdit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }
        private void LoadData()
        {

            dgvList.DataSource = loadingRepo.GetLoadingList();
            dgvList.Columns["LoadID"].HeaderText = "ID";
            dgvList.Columns["Professor"].HeaderText = "Преподаватель";
            dgvList.Columns["Group"].HeaderText = "Группа";
            dgvList.Columns["Hours"].HeaderText = "Количество часов";


            //dgvList.Columns["LoadID"].Visible = false;


            dgvList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }

        private void frmLoadingList_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                Loading loading = (dgvList.SelectedRows[0].DataBoundItem as Loading);
                frmLoadingEdit loadingEdit = new frmLoadingEdit(loadingRepo, loading);
                if (loadingEdit.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmLoadingEdit loadingEdit = new frmLoadingEdit(loadingRepo);
            if (loadingEdit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }
    }
}
