﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmPaymentEdit : Form
    {
        Payment payment;
        PaymentRepository paymentRepo;
        public frmPaymentEdit()
        {
            InitializeComponent();
        }
        public frmPaymentEdit(PaymentRepository _paymentRepo)
        {
            InitializeComponent();
            paymentRepo = _paymentRepo;
        }

        public frmPaymentEdit(PaymentRepository _paymentRepo, Payment _payment)
        {
            InitializeComponent();
            paymentRepo = _paymentRepo;
            payment = _payment;


            if (payment != null)
            {
                txtPayment.Text = payment.PayPerHour.ToString();
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckData())
            {
                if (payment == null)
                {
                    payment = new Payment();

                    payment.PayPerHour = Convert.ToInt32(txtPayment.Text);

                    paymentRepo.AddPayment(payment);
                }
                else
                {
                    payment.PayPerHour = Convert.ToInt32(txtPayment.Text);

                    paymentRepo.EditPayment(payment);
                }
            }
            else
            {
                MessageBox.Show("Данные внесены некорректно!");
            }
        }

        private bool CheckData()
        {
            bool result = true;
            
            string str = txtPayment.Text;
            int pay = 0;
            if (int.TryParse(str, out pay))
            {
                if (pay < 0 )
                    result = false;
            }
            else
            {
                result = false;
            }
            return result;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
