﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmPaymentList : Form
    {
        PaymentRepository paymentRepo = new PaymentRepository();
        public frmPaymentList()
        {
            InitializeComponent();
        }

        private void LoadData()
        {

            dgvList.DataSource = paymentRepo.GetpaymentList();
            dgvList.Columns["PaymentID"].HeaderText = "ID";
            dgvList.Columns["PayPerHour"].HeaderText = "Оплата";

            dgvList.Columns["PaymentID"].Visible = false;


            dgvList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmPaymentEdit paymentEdit = new frmPaymentEdit(paymentRepo);
            if (paymentEdit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            EditRecord();
        }

        private void EditRecord()
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                Payment payment= (dgvList.SelectedRows[0].DataBoundItem as Payment);
                frmPaymentEdit paymentEdit = new frmPaymentEdit(paymentRepo, payment);
                if (paymentEdit.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
            }
        }

        private void frmPaymentList_Load(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}
