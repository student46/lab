﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;

namespace CourseAutomatization
{
    public partial class frmProfEdit : Form
    {
        Professor prof;
        ProfessorRepository professorRepo;
        public frmProfEdit(ProfessorRepository _profRepo)
        {
            InitializeComponent();
            professorRepo = _profRepo;
        }

        public frmProfEdit(ProfessorRepository _profRepo, Professor _prof)
        {
            InitializeComponent();
            prof = _prof;
            professorRepo = _profRepo;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckData())
            {
                if (prof == null)
                {
                    prof = new Professor();

                    prof.FirstName = txtSurname.Text;
                    prof.LastName = txtName.Text;
                    prof.Patronimyc = txtPatronimyc.Text;
                    prof.Age = Convert.ToInt32(txtAge.Text);
                    prof.Experience = Convert.ToInt32(txtExperience.Text);
                    prof.PaspSeries = txtPaspSeries.Text;
                    prof.PaspNumber = txtPaspNumber.Text;
                    prof.Address = txtAddress.Text;
                    prof.Phone = txtPhone.Text;


                    professorRepo.AddProfessor(prof);
                }
                else
                {
                    prof.FirstName = txtSurname.Text;
                    prof.LastName = txtName.Text;
                    prof.Patronimyc = txtPatronimyc.Text;
                    prof.Age = Convert.ToInt32(txtAge.Text);
                    prof.Experience = Convert.ToInt32(txtExperience.Text);
                    prof.PaspSeries = txtPaspSeries.Text;
                    prof.PaspNumber = txtPaspNumber.Text;
                    prof.Address = txtAddress.Text;
                    prof.Phone = txtPhone.Text;

                    professorRepo.EditProfessor(prof);
                }
            }
            else
            {
                MessageBox.Show("Данные внесены некорректно!");
            }
        }

        private bool CheckData()
        {
            bool result = true;
            if (txtSurname.Text.Trim() == "")
                result = false;
            if (txtName.Text.Trim() == "")
                result = false;
            if (txtPatronimyc.Text.Trim() == "")
                result = false;
            if (txtPaspSeries.Text.Trim() == "")
                result = false;
            if (txtPaspNumber.Text.Trim() == "")
                result = false;
            if (txtAddress.Text.Trim() == "")
                result = false;
            if (txtPhone.Text.Trim() == "")
                result = false;

            string str = txtAge.Text;
            int age = 0;
            if (int.TryParse(str, out age))
            {
                if (age < 0 || age > 200)
                    result = false;
            }
            else
            {
                result = false;
            }
            str = txtExperience.Text;
            int exp = 0;
            if (int.TryParse(str, out exp))
            {
                if (exp < 0 || exp > 200)
                    result = false;
            }
            else
            {
                result = false;
            }
            return result;
        }
    }
}
