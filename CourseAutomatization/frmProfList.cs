﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CourseAutomatization.Repository;
using CourseAutomatization.Entity;


namespace CourseAutomatization
{
    public partial class frmProfList : Form
    {
        ProfessorRepository professorRepo = new ProfessorRepository();
        public frmProfList()
        {
            InitializeComponent();
        }

        private void LoadData()
        {

            dgvList.DataSource = professorRepo.GetProfList();
            dgvList.Columns["ProfID"].HeaderText = "ID";
            dgvList.Columns["FirstName"].HeaderText = "Фамилия";
            dgvList.Columns["LastName"].HeaderText = "Имя";
            dgvList.Columns["Patronimyc"].HeaderText = "Отчество";
            dgvList.Columns["Age"].HeaderText = "Возраст";
            dgvList.Columns["Experience"].HeaderText = "Стаж";
            dgvList.Columns["PaspSeries"].HeaderText = "Серия паспорта";
            dgvList.Columns["PaspNumber"].HeaderText = "Номер паспорта";

            dgvList.Columns["Address"].HeaderText = "Адрес";
            dgvList.Columns["Phone"].HeaderText = "Телефон";

            dgvList.Columns["ProfID"].Visible = false;


            dgvList.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
        }
        private void frmProfList_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            frmProfEdit profEdit = new frmProfEdit(professorRepo);
            if (profEdit.ShowDialog() == DialogResult.OK)
            {
                LoadData();
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

            EditRecord();
        }

        private void EditRecord()
        {
            if (dgvList.SelectedRows.Count == 1)
            {
                Professor prof = (dgvList.SelectedRows[0].DataBoundItem as Professor);
                frmProfEdit profEdit = new frmProfEdit(professorRepo, prof);
                if (profEdit.ShowDialog() == DialogResult.OK)
                {
                    LoadData();
                }
            }
        }
    }
}
